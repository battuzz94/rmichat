package download;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface Server extends Remote {
	
	// invia al client tutti i messaggi che deve ancora leggere.
	public ArrayList<String> receive(int id) throws RemoteException;
	
	// ridistribuisce il messaggio inviato dal client a tutti gli altri client connessi.
	public void dispatch(String msg, int id) throws RemoteException;
	
	// Restituisce un id al client per identificarlo.
	public int getId() throws RemoteException;
}
