package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ChatServer {
	public static void main(String[] args) {
		// Imposta il security manager
//		if (System.getSecurityManager() == null) {
//            System.setSecurityManager(new SecurityManager());
//        }
		
		System.out.println("Ciao, benvenuto in questa chat!");
		
		try {
//			HelloServerImpl server = new HelloServerImpl();
			ChatServerImpl server = new ChatServerImpl();
			
			Registry registry = errorCheckedGetRegistry();
			
			registry.rebind(REMOTE_OBJECT_KEY, server);
			
			System.out.println("Il server è stato avviato. In attesa di qualche connessione..");
			
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * Questa funzione cerca prima un registro locale. Nel caso non sia inizializzato, prova
	 * a crearlo da se. Nel caso non sia possibile crearlo il programma termina
	 * @return Il registro locale ottenuto o creato.
	 */
	private static Registry errorCheckedGetRegistry() {
		Registry registry = null;
		try {
			registry = LocateRegistry.getRegistry();
		}
		catch (RemoteException e) {
			// Il registro non esiste ancora
			System.out.println("Il registro non è ancora avviato. Provo a crearlo..");
			try {
				registry = LocateRegistry.createRegistry(DEFAULT_REGISTRY_PORT);
			}
			catch (RemoteException e2) {
				System.out.println("Impossibile creare il registro.");
				e2.printStackTrace();
				System.exit(1);
			}
		}
		return registry;
	}
	
	
	private static final int DEFAULT_REGISTRY_PORT = 1099;
	private static final String REMOTE_OBJECT_KEY = "ChatServer";
}
