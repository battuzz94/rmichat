package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import download.Server;

/**
 * Questa è una semplice implementazione del server che permette ai client di chattare
 * tra loro.
 * @author Andrea
 *
 */

public class ChatServerImpl extends UnicastRemoteObject implements Server {

	public ChatServerImpl() throws RemoteException {
		numberOfClients = 0;
		messages = new ArrayList<ArrayList<String>>();
	}
	
	@Override
	public ArrayList<String> receive(int id) throws RemoteException {
		ArrayList<String> msgToDeliver;
		try {
			msgToDeliver = messages.get(id);
			messages.set(id, new ArrayList<String>()); // Cancella dalla 'coda' i messaggi del client
		} 
		catch (IndexOutOfBoundsException e) {
			System.out.println("Id non valido");
			msgToDeliver = new ArrayList<String>();	// Se l'id non è valido restituisce una lista vuota
		}
	
		return msgToDeliver;
	}

	@Override
	public void dispatch(String msg, int id) throws RemoteException {
		
		for (int i = 0; i < numberOfClients; i++) {
			if (i != id) {
				try {
					messages.get(i).add(msg);
				}
				catch (IndexOutOfBoundsException e) {
					System.out.println("Id non valido");
				}
			}
		}
		System.out.println("[DEBUG] Messaggio ricevuto: stato messages: " + messages);
	}
	
	public int getId() {
		System.out.println("[DEBUG] Si è connesso un nuovo client");
		
		numberOfClients++;
		
		// Aggiunge una nuova riga alla matrice dei messaggi.
		messages.add(new ArrayList<String>());
		
		// Ogni volta al client viene associato l'ultima posizione liberata 0-based
		return numberOfClients-1; 
	}
	
	
	private int numberOfClients;
	
	// È una matrice di stringhe. Alla riga i-esima ci saranno tutti i messaggi da recapitare al client con id uguale a i
	private ArrayList<ArrayList<String>> messages;
}
