package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import download.Server;

/**
 * Questa è una semplice implementazione del server che saluta i client ad ogni richiesta.
 * @author Andrea
 *
 */

public class HelloServerImpl extends UnicastRemoteObject implements Server {
	private int current_id;
	public HelloServerImpl() throws RemoteException {
		current_id = 0;
	}
	@Override
	public ArrayList<String> receive(int id) throws RemoteException {
		ArrayList<String> msgs = new ArrayList<String>();
		msgs.add("Hello!");
		
		
		return msgs;
	}

	@Override
	public void dispatch(String msg, int id) throws RemoteException {
		
	}
	
	public int getId() {
		current_id++;
		return current_id;
	}

}
