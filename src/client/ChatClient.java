package client;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Scanner;

import download.Server;


/*
 * Il client si limita a eseguire due thread. Il primo cicla per leggere stringhe da tastiera
 * e manda al server appena riceve una nuova linea. Il secondo cicla interrogando il server 
 * per sapere se ci sono nuovi messaggi da stampare a video.
 */
public class ChatClient {
	
	
	
	
	public static void main(String[] args) {
		// Imposta il security manager
//		if (System.getSecurityManager() == null) {
//            System.setSecurityManager(new SecurityManager());
//        }
		
		try {
			Registry registry = LocateRegistry.getRegistry();
			
			// Supponiamo che il server sia già avviato
			Server server = (Server) registry.lookup("ChatServer");
			
			int id = -1;
			try {
				id = server.getId(); 
			}
			catch (RemoteException e) {
				System.out.println("Impossibile ottenere un id dal server");
				e.printStackTrace();
				System.exit(1);
			}
			
			Thread tSend = new Thread(new SendThread(server, id));
			Thread tRecv = new Thread(new ReceiveThread(server, id));
			
			tSend.start();
			tRecv.start();
			
			boolean running = true;
			while (running) {
				try {
					Thread.sleep(1000);
				}
				catch (InterruptedException e) {
					tSend.interrupt();
					tRecv.interrupt();
					running = false;
				}
			}
		}
		catch (AccessException e) {
			System.out.println("Non è possibile accedere all'oggetto remoto");
			e.printStackTrace();
		}
		catch (RemoteException e) {
			System.out.println("Impossibile localizzare il registro. Assicurati di aver avviato il server e riprovare.");
			e.printStackTrace();
		}
		catch (NotBoundException e){
			System.out.println("Il server non è ancora avviato. Avviare il server e riprovare");
		}
	}
}

/*
 * Thread che cicla e manda una dispatch al server ogni volta che legge una nuova riga
 * dall'input.
 */

class SendThread implements Runnable {
	Server server;
	int id;

	public SendThread(Server server, int id) {
		this.server = server;
		this.id = id;
	}
	
	public void run() {
		Scanner in = new Scanner(System.in);
		while (true) {
			String s = in.nextLine();
			try {
				synchronized(server) {
					server.dispatch(s, id);
				}
			} catch (RemoteException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
}

/*
 * Questo thread cicla e chiede al server se ci sono nuovi messaggi ogni REFRESH_RATE millisecondi
 */
class ReceiveThread implements Runnable {
	Server server;
	int id;
	
	public ReceiveThread(Server server, int id) {
		this.server = server;
		this.id = id;
	}
	
	public void run() {
		while (true) {
			try {
				ArrayList<String> msgs;
				synchronized (server) {
					msgs = server.receive(id);
				}
				
				for (String s : msgs)
					System.out.println(s);
				
				Thread.sleep(REFRESH_RATE);
				
			} catch (RemoteException e) {
				System.out.println("Il server si è disconnesso\nCiao e grazie per aver usato la chat!");
				System.exit(1);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private static final int REFRESH_RATE = 500; // controlla ogni mezzo secondo
}