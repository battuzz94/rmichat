# README #

Istruzioni per compilare e avviare il programma in locale:

* Scaricare o clonare il repository
* Aprire un terminale e posizionarsi all'interno della cartella src/
* Compilare i file .java con javac:
```
#!bash

javac client/*.java
javac server/*.java
```
* Avviare il registro con il comando:

```
#!bash

rmiregistry &
```

* Avviare il server con il comando: 
```
#!bash

java server.ChatServer
```

* Avviare tutti i client che si vuole, in altri terminali, con il comando:
```
#!bash

java client.ChatClient
```